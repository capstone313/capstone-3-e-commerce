import { Row, Col, Button } from 'react-bootstrap';
import { Routes, Route } from 'react-router-dom';
import { Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';


export default function Banner() {

	
	return (

		<Row name="errorBanner">
			<Col className="p-5">
				<h1>Error 404 - Page Not Found</h1>
				<p>The page you are looking for cannot be found</p>
				<Button variant="primary" as={Link} to="/">Back to Home</Button>
			</Col>
		</Row>

	)
}