import { useState, useEffect, useContext} from 'react';
import { Button, Container, Table, Row, Col } from 'react-bootstrap';
import { Link, useParams }  from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function ProductTable(){

	const { user } = useContext(UserContext);

	const [per, setPer] = useState();
	const [products, setProducts] = useState([]);
	const [isArchive, setArchive] = useState(false);
	const [btnValue, setBtnValue] = useState(false);
	const [count, setCount] = useState(0);


	const archiveProduct = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
					isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){				
				Swal.fire({
					title: "Product has been archived!",
					icon: "info"
				}).then(() => {
					window.location.reload();
				})
			} else {
				Swal.fire({
					title: "An error has occured!",
					icon: "error"
				})
			}
		})
	}

	const unarchiveProduct = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
					isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				
				Swal.fire({
					title: "Product has been unarchived!",
					icon: "info"
				}).then(() => {
					window.location.reload();
				})
			} else {
				Swal.fire({
					title: "An error has occured!",
					icon: "error"
				})
			}
		})
	}

	useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				const sortProducts = data.sort((a, b) => (a.productName > b.productName) ? 1 : -1);
				setProducts(sortProducts);
			})
	}, [])
	
	return (

		<Table hover bordered>
			<thead>
				<tr>
					<th className="text-center">#</th>
					<th className="text-center">Product Name</th>
					<th className="text-center">Description</th>
					<th className="text-center">Category</th>
					<th className="text-center">Price</th>
					<th className="text-center">Availability</th>
					<th className="text-center" colSpan={2}>Actions</th>
				</tr>
			</thead>
			<tbody>
				{products.map((product, i) => (
					<tr key={product._id}>
						<td className="text-center">{i + 1}</td>
						<td className="text-center">{product.productName}</td>
						<td className="text-center">{product.productDescription}</td>
						<td className="text-center">{product.category}</td>
						<td className="text-center">{product.unitPrice}</td>
						<td className="text-center">{product.isActive.toString()}</td>
						<td className="text-center"><Button variant="outline-warning" as={Link} to={`update/${product._id}`}>Edit</Button></td>
						<td className="text-center">
							{(product.isActive === true) ?
								<Button variant="outline-success" onClick={() => archiveProduct(product._id, false)}>Archive</Button> 
								:
								<Button variant="outline-danger" onClick={() => unarchiveProduct(product._id, true)}>Unarchive</Button>
							}
						</td>
					</tr>					
				))}
			</tbody>			
		</Table>
	)
}