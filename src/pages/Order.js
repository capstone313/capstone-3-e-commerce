import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

import AllOrders from '../components/AllOrders';
import Banner from '../components/Banner';
import Cart from '../components/Cart';

export default function Order() {

	const { user, setUser } = useContext(UserContext);

	const [data, setData] = useState('');

	const userOrders = {
		title: "Your Orders",
		content: ""	
	}

	const allOrders = {
		title: "All Orders",
		content: ""	
	}

	useEffect(() => {
		if(user.isAdmin){
			setData(allOrders)
		} else {
			setData(userOrders)
		}
	}, [])

	
	return (

		(user.isAdmin) ?
		<>
			<Banner data={data} />
			<AllOrders />
		</>
		:
		<>
			<Banner data={data} />
			<Cart />
		</>
	)
}