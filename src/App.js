import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';

import AddProduct from './pages/AddProduct';
import Dashboard from './pages/Dashboard';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Order from "./pages/Order";
import Register from './pages/Register';
import UpdateProduct from './pages/UpdateProduct';
import './App.css';
import { UserProvider } from './UserContext';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null

  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
      // console.log(res.status)
      
    .then(data => {
      console.log(data)

      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else { 
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar />
          <Container>
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path='/dashboard' element={<Dashboard />} />
                <Route path='/addproduct' element={<AddProduct />} />
                <Route path="/dashboard/update/:productId" element={<UpdateProduct/>} />
                <Route path="/products" element={<Products />} /> 
                <Route path="/products/:productId" element={<ProductView />} /> 
                <Route path="/orders" element={<Order />} /> 
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/*" element={<Error />} />              
              </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
